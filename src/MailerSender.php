<?php
namespace AmsomUtilities;

use Kreait\Firebase\RemoteConfig\Template;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
class MailerSender
{
    private MailerInterface $mailer;
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }
    
    public function sendMail($mail, $subject, $template, $data, $from = null){
        if(!$from && array_key_exists('MAILER_DEFAULT_FROM', $_ENV)){
            $from = $_ENV['MAILER_DEFAULT_FROM'];
        }

        if(!$from){
            $from = 'nepasrepondre@amsom-habitat.fr';
        }

        if (array_key_exists('MAIL_ENV', $_ENV) && $_ENV['MAIL_ENV'] == 'prod')
            $mailTo = $mail;
        else {
            $mailTo = "dev@amsom-habitat.fr";
        }

        if (!array_key_exists('MAIL_ENV', $_ENV) || $_ENV['MAIL_ENV'] != 'prod')
            $subject = 'Test API - ' . $subject;

        $email = (new TemplatedEmail())
            ->from($from)
            ->to($mailTo)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($data);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            throw new HttpException(500, $e->getMessage());
        }
    }
    
//    $route = "send_custom";
//    $destinataire = "y.gillot@amsom-habitat.fr";
//    $subject = "Test API";
//    $body = ["message" => "Ceci est un test de l'API"];
//    $base64 = "JVBERi0xLjEKJcKlwrHDqwoKMSAwIG9iagogIDw8IC9UeXBlIC9DYXRhbG9nCiAgICAgL1BhZ2VzIDIgMCBSCiAgPj4KZW5kb2JqCgoyIDAgb2JqCiAgPDwgL1R5cGUgL1BhZ2VzCiAgICAgL0tpZHMgWzMgMCBSXQogICAgIC9Db3VudCAxCiAgICAgL01lZGlhQm94IFswIDAgMzAwIDE0NF0KICA+PgplbmRvYmoKCjMgMCBvYmoKICA8PCAgL1R5cGUgL1BhZ2UKICAgICAgL1BhcmVudCAyIDAgUgogICAgICAvUmVzb3VyY2VzCiAgICAgICA8PCAvRm9udAogICAgICAgICAgIDw8IC9GMQogICAgICAgICAgICAgICA8PCAvVHlwZSAvRm9udAogICAgICAgICAgICAgICAgICAvU3VidHlwZSAvVHlwZTEKICAgICAgICAgICAgICAgICAgL0Jhc2VGb250IC9UaW1lcy1Sb21hbgogICAgICAgICAgICAgICA+PgogICAgICAgICAgID4+CiAgICAgICA+PgogICAgICAvQ29udGVudHMgNCAwIFIKICA+PgplbmRvYmoKCjQgMCBvYmoKICA8PCAvTGVuZ3RoIDU1ID4+CnN0cmVhbQogIEJUCiAgICAvRjEgMTggVGYKICAgIDAgMCBUZAogICAgKEhlbGxvIFdvcmxkKSBUagogIEVUCmVuZHN0cmVhbQplbmRvYmoKCnhyZWYKMCA1CjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDAxOCAwMDAwMCBuIAowMDAwMDAwMDc3IDAwMDAwIG4gCjAwMDAwMDAxNzggMDAwMDAgbiAKMDAwMDAwMDQ1NyAwMDAwMCBuIAp0cmFpbGVyCiAgPDwgIC9Sb290IDEgMCBSCiAgICAgIC9TaXplIDUKICA+PgpzdGFydHhyZWYKNTY1CiUlRU9GCg==";
//    $attachment = [["content" => "https://picsum.photos/200/300", "type" => "path", "filename" => "urlFile"], ["content" => $base64, "type" => "file", "filename" => "base64File"]];
//    $replyTo = "y.gillot@amsom-habitat.fr"
//    $test = $mailerSender->apiSendMail($route, $destinataire, $subject, $body, null,$attachment, true, $replyTo);
//
//    return new Response(
//        $test,
//        Response::HTTP_OK,
//        [
//            'content-type' => 'application/json',
//            'Access-Control-Allow-Origin' => '*'
//        ]
//    );
    public function apiSendMail($route, $mailDestinataire, $cc, $cci, $subject, $body, $destinataire = null, $attachment = null, $showRemerciement = true, $replyTo = null, $signature = null) : string
    {
        if (array_key_exists('MAIL_ENV', $_ENV) && $_ENV['MAIL_ENV'] == 'prod')
            $mailTo = $mailDestinataire;
        else {
            $mailTo = "dev@amsom-habitat.fr";
        }
        // if (!array_key_exists('MAIL_ENV', $_ENV) || $_ENV['MAIL_ENV'] != 'prod')
        //     $subject = 'Test API - ' . $subject;
//        convert all attachment with type === path to base64 and add extension to filename
        if($attachment){
            foreach ($attachment as $key => $value){
                if($value['type'] === 'path'){
                    $attachment[$key]['content'] = base64_encode(file_get_contents($value['content']));
                    $attachment[$key]['type'] = 'file';
                }
                $haveExtension = explode('.', $value['filename']);
                if(count($haveExtension) == 1){
                    $imgdata = base64_decode($attachment[$key]['content']);
                    $f = finfo_open();
                    //get extension of file
                    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
                    $extension = explode('/', $mime_type)[1];
                    $attachment[$key]['filename'] = $value['filename'] . '.' . $extension;
                }
            }
        }
        //call post to api to send mail
        $url = $_ENV['API_MAIL_URL'] . '/' . $route;
        $data = [
            "emailDestinataire" => $mailTo,
            "replyTo" => $replyTo,
            "cc" => $cc,
            "cci" => $cci,
            "destinataire" => $destinataire,
            "sujet" => $subject,
            "body" => $body,
            "attachement" => $attachment,
            "showRemerciement" => $showRemerciement,
            "signature" => $signature
        ];
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public function apiSendMailCustom($mailDestinataire, $cc, $cci, $subject, $body, $destinataire = null, $attachment = null, $replyTo = null, $signature = null, $templateConfig = [], $remerciementConfig = []) : string
    {
        if (array_key_exists('MAIL_ENV', $_ENV) && $_ENV['MAIL_ENV'] == 'prod')
            $mailTo = $mailDestinataire;
        else {
            $mailTo = "dev@amsom-habitat.fr";
        }
        // Convert all attachment with type === path to base64 and add extension to filename
        if($attachment){
            foreach ($attachment as $key => $value){
                if($value['type'] === 'path'){
                    $attachment[$key]['content'] = base64_encode(file_get_contents($value['content']));
                    $attachment[$key]['type'] = 'file';
                }
                $haveExtension = explode('.', $value['filename']);
                if(count($haveExtension) == 1){
                    $imgdata = base64_decode($attachment[$key]['content']);
                    $f = finfo_open();
                    //get extension of file
                    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
                    $extension = explode('/', $mime_type)[1];
                    $attachment[$key]['filename'] = $value['filename'] . '.' . $extension;
                }
            }
        }
        //call post to api to send mail
        $url = $_ENV['API_MAIL_URL'] . '/send_custom';
        $data = [
            "emailDestinataire" => $mailTo,
            "replyTo" => $replyTo,
            "cc" => $cc,
            "cci" => $cci,
            "destinataire" => $destinataire,
            "sujet" => $subject,
            "body" => $body,
            "attachement" => $attachment,
            "signature" => $signature,
            "logo" => $templateConfig['logo'] ?? "logoAmsomEtMoi",
            "headerTemplateName" => $templateConfig['headerTemplateName'] ?? null,
            "bodyTemplateName" => $templateConfig['bodyTemplateName'] ?? null,
            "footerMiddlePart" => $remerciementConfig['footerMiddlePart'] ?? null,
            "footerTemplateName" => $templateConfig['footerTemplateName'] ?? null,
            "showFooter" => $templateConfig['showFooter'] ?? true,
            "showRemerciement" => $remerciementConfig['showRemerciement'] ?? true,
            "remerciement" => $remerciementConfig['remerciement'] ?? null,
            "auteur" => $remerciementConfig['auteur'] ?? null
        ];
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}